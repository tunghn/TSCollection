//
//  TSBaseCollectionViewCell.swift
//  TSCollection
//
//  Created by sanghv on 10/19/16.
//  Copyright © 2016 TS. All rights reserved.
//

import UIKit

open class TSBaseCollectionViewCell: UICollectionViewCell {
    
    deinit {

    }

    open weak var delegate: AnyObject?
    open var indexPath: IndexPath?

    override public init(frame: CGRect) {
        super.init(frame: frame)

        self.configView()
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override open func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        self.configView()
    }

    open func setIndexPath(indexPath: IndexPath?, sender: AnyObject?) {
        self.indexPath = indexPath
        self.delegate = sender
    }
}

@objc extension TSBaseCollectionViewCell {

    open func configView() {

    }
}

@objc public extension TSBaseCollectionViewCell {

    // MARK: - Reuse identifer

    class var identifier: String {
        get {
            let mirror = Mirror(reflecting: self)
            return "\(String(describing: mirror.subjectType).replacingOccurrences(of: ".Type", with: ""))ID"
        }
    }
}

@objc extension TSBaseCollectionViewCell: TSCellDatasource {

    // MARK: - TSCellDatasource

    open class var cellIdentifier: String {
        get {
            return "CellIdentifier"
        }
    }

    open func configCellWithData(data: Any?) {

    }
}
