//
//  TSDateFormatter.swift
//  TSCollection
//
//  Created by sanghv on 10/19/16.
//  Copyright © 2016 TS. All rights reserved.
//

import Foundation

/** TSDateFormatter Class

*/
public let fullDateTimeFormatter = TSDateFormatter.shared.fullDateTimeFormatter
public let fullDateFormatter = TSDateFormatter.shared.fullDateFormatter
public let invariantCultureFormatter = TSDateFormatter.shared.invariantCultureFormatter

open class TSDateFormatter {

    open static let shared = TSDateFormatter()

    private init(){}

    lazy open var fullDateTimeFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

        return dateFormatter
    }()

    lazy open var fullDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"

        return dateFormatter
    }()

    lazy open var invariantCultureFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"

        return dateFormatter
    }()
    
    lazy open var fullDateTimeSystemFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone.system
        
        return dateFormatter
    }()

    lazy open var fullDateTimeUTCFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC") as TimeZone!

        return dateFormatter
    }()
}
